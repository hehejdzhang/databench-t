/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.common.constant;
/**
 * 交易配置常量
 *
 */
public class ChkuplogConstant {
	/**
	 * 对账类型原子性（Atomicity）
	 */
	public static final String CHKUPLOG_TYPE_A = "A";
	/**
	 * 对账类型一致性（Consistency）
	 */
	public static final String CHKUPLOG_TYPE_C = "C";
	/**
	 * 对账类型隔离性（Isolation）
	 */
	public static final String CHKUPLOG_TYPE_I = "I";
	/**
	 * 对账类型持久性（Durability）
	 */
	public static final String CHKUPLOG_TYPE_D = "D";

}
