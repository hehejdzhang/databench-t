/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.mapper.DataCleanMapper;
import com.service.DataCleanService;

@Service
@Scope("prototype")
public class DataCleanServiceImpl implements DataCleanService,Runnable{

    private final Logger logger = LoggerFactory.getLogger(DataCleanServiceImpl.class); 

    @Autowired
    DataCleanMapper dataCleanMapper;
	public void run() {
		try {
			logger.info(" DataCleanMapper begin cleanChkuplog ");
			dataCleanMapper.cleanChkuplog();
			logger.info(" DataCleanMapper end cleanChkuplog ");
			logger.info(" DataCleanMapper begin cleanTranlist ");
			dataCleanMapper.cleanTranlist();
			logger.info(" DataCleanMapper end cleanTranlist ");
		} catch (Exception e) {
			logger.error(" DataCleanServiceImpl run error {} ",e);
		}
	}

    
}